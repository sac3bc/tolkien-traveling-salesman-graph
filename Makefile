#Sesi Cadmus  Lab Section 2PM Lab 10 Makefile 
CXX=clang++ #$ (CXXFLAGS)
CXXFLAGS=-Wall
DEBUG=-Wall -g
.SUFFIXES: .o .cpp
OFILES = middleearth.o traveling.o

main:   $(OFILES) #tabbed in has all O files
    $(CXX) $(DEBUG) $(OFILES) #has how it will run the executable
    doxygen



middleearth.o: middleearth.cpp middleearth.h
trav.o: traveling.cpp middleearth.h

clean:
    /bin/rm -f *.o *~
