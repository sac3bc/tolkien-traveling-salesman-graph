#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

/*! 
  \author Sesi Cadmus
  \details (additional code provided by Aaron Bloomfield)
  \date 11-29-16
  
  This code implements a topological, given information for a graph  

 */




/*! \brief

  This is the .cpp file that contains the main() for middleearth.h and middleearth.cpp Given a MiddleEarth and a list of destinations, this file will compute the shortest path to travel between cities for the traveling salesman problem
 */


using namespace std;

#include "middleearth.h"


/*! \brief computeDistance: Computes the distance between each city in a list of cities 
 *  computeDistance takes in a MiddleEarth, string, and vector of string destinations 
 *  Goes through and sums up the total amount it woudl take to cycle through the cities, in order
 * 
 */
float computeDistance (MiddleEarth &me, string start, vector<string> dests);


/*! \brief printRoute: takes in a list of destinations and prints the route
 *        iterates through the list, printing the cycle, and making sure to print the start position at the end to complete the cycle
 *
 *  
 */

void printRoute (string start, vector<string> dests);

int main (int argc, char **argv) {
  
    // check the number of parameters
    if ( argc != 6 ) {
        cout << "Usage: " << argv[0] << " <world_height> <world_width> "
             << "<num_cities> <random_seed> <cities_to_visit>" << endl;
        exit(0);
    }
    
    // we'll assume the parameters are all well-formed
    int width, height, num_cities, rand_seed, cities_to_visit;
    sscanf (argv[1], "%d", &width);
    sscanf (argv[2], "%d", &height);
    sscanf (argv[3], "%d", &num_cities);
    sscanf (argv[4], "%d", &rand_seed);
    sscanf (argv[5], "%d", &cities_to_visit);
  
    // Create the world, and select your itinerary
    MiddleEarth me(width, height, num_cities, rand_seed);
    vector<string> dests = me.getItinerary(cities_to_visit);
    // YOUR CODE HERE
   
    
    //initial min  distance is a large number

    
    float min = 99999999999999999999999999999999999999999999999.0;
    //have vector to store the combination that produced the min distance
    vector<string> tempMin = dests;
    //sort the combinations 
    sort(dests.begin() + 1, dests.end());
    //go through each combination, and compare its distance to the min distance
    while(next_permutation(dests.begin() + 1, dests.end())){
    
    float x = computeDistance(me, dests[0], dests); 
      
    if(x < min){
      min = x;
      tempMin = dests;
    }
      }
    
     
    cout << "Your journey will take you along the path ";
    printRoute(tempMin[0], tempMin);
    cout << endl;
    cout << "and have length " << min << endl; 
            
               
    return 0;


    
}
// This method will compute the full distance of the cycle that starts
// at the 'start' parameter, goes to each of the cities in the dests
// vector IN ORDER, and ends back at the 'start' parameter.

float computeDistance (MiddleEarth &me, string start, vector<string> dests) {
    // YOUR CODE HERE
  float total = 0.0;
  
  string city1;
  string city2;

 for(int i = 0; i < dests.size() - 1; i ++){
   city1 = dests[i];
   city2 = dests[i + 1];
  total += me.getDistance(city1, city2);
   }

  //add from looping back around
  total += me.getDistance(dests[0], dests[dests.size() -1]);

  return total;
    
}

// This method will print the entire route, starting and ending at the
// 'start' parameter.  The output should be of the form:
// Erebor -> Khazad-dum -> Michel Delving -> Bree -> Cirith Ungol -> Erebor
void printRoute (string start, vector<string> dests) {
      
for(int i = 0; i < dests.size(); i++){
  cout << dests[i] << " -> ";
 }
 cout << dests[0];
 cout << endl; 

}

